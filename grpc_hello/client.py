from grpc_hello.proto import helloworld_pb2_grpc, helloworld_pb2
import grpc

if __name__ == '__main__':
    with grpc.insecure_channel("localhost:8080") as channel:
        stub = helloworld_pb2_grpc.GreeterStub(channel)
        rsp: helloworld_pb2.HelloReply = stub.SayHello(helloworld_pb2.HelloRequest(name="bobby"))

        print(rsp.message)