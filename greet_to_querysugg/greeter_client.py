import o2oalgo_querysugg_pb2
import o2oalgo_querysugg_pb2_grpc
import grpc

args = {
  "user_id": 24,
  "query": "Tea",
  "scene": 0,
  "ab_test": "dsa=xxx,ls.suggestion_dict=test2",
  "city_id": 51,
  "publish_id": "15e412f8-a462-43a2-8f12-99d93c919a0f"
}

addr = '10.144.56.125:8080'

def run():
    with grpc.insecure_channel(addr) as channel:
        stub = o2oalgo_querysugg_pb2_grpc.O2OalgoQuerysuggStub(channel)
        resp = stub.GetLsQuerySuggestList(o2oalgo_querysugg_pb2.GetLsQuerySuggestListReq(user_id=args["user_id"], \
                                                                                         query=args["query"], scene=args["scene"], ab_test=args["ab_test"], city_id=args["city_id"], publish_id=args["publish_id"]))
        print(resp.query_suggest_list)

if __name__ == '__main__':
    run()
